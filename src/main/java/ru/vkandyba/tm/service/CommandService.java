package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.ICommandRepository;
import ru.vkandyba.tm.model.Command;

public class CommandService implements ru.vkandyba.tm.api.ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
